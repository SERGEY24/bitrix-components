<? 
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("MAIN_COMPONENT_NAME"),
	"DESCRIPTION" => GetMessage("MAIN_COMPONENT_DESCR"),
	"ICON" => "/images/bxSlider.png",
	"PATH" => array(
		"ID" => "utility",
	),
);
?>