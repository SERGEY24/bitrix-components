<?php

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if (isset($arParams["MEDIALIB"])) {

	if (\Bitrix\Main\Loader::includeModule('fileman')) {
		
		CMedialib::Init();
		
		$idCollection = $arResult['ID'] = intval($arParams["MEDIALIB"]);
		$arCollectionsItem = CMedialibItem::GetList(array('arCollections' => array("0" => $idCollection)));
		
		if (!empty($arCollectionsItem)) {
			
			$CHARACTER_CODE = 'MEDIALIB';	
			$arParams["CHARACTER_CODE"] = $CHARACTER_CODE;
			$arCollectionId = array();
			
			foreach ($arCollectionsItem as $arCollection) {
				$arCollectionId[] = $arCollection['SOURCE_ID'];
			}
			
			$arParams['PROPERTIES'][$CHARACTER_CODE]['VALUE'] = $arCollectionId; 		
		}
	}
}

if (isset($arParams['PROPERTIES']) && !empty($arParams['PROPERTIES'])) {
	
	$CHARACTER_CODE = $arParams["CHARACTER_CODE"];

	if (empty($arResult['ID'])) 
		$arResult['ID'] = $CHARACTER_CODE;
	
	if (isset($arParams['PROPERTIES'][$CHARACTER_CODE]) && !empty($arParams['PROPERTIES'][$CHARACTER_CODE]['VALUE'])) {	
	
		$BIG_SIZE = $arParams["BIG_SIZE"];
		$PREW_SIZE = $arParams["PREW_SIZE"];
		
		$arResult['MODAL_WINDOW'] = $arParams['MODAL_WINDOW'];
				
		if (empty($arParams['RESIZE_TYPE']))
			$arParams['RESIZE_TYPE'] = 0;
		
		$RESIZE_TYPE_ARR = array(
			BX_RESIZE_IMAGE_EXACT,
			BX_RESIZE_IMAGE_PROPORTIONAL,
			BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
		);
		
		if (!empty($BIG_SIZE)) {
			$BIG_SIZE_ARR = explode('x', trim($BIG_SIZE));
			$bigWidth = trim($BIG_SIZE_ARR[0]);
			$bigHeight = trim($BIG_SIZE_ARR[1]);
		}
		
		if (!empty($PREW_SIZE)) {
			$PREW_SIZE_ARR = explode('x', trim($PREW_SIZE));
			$prewWidth = trim($PREW_SIZE_ARR[0]);
			$prewHeight = trim($PREW_SIZE_ARR[1]);
		}

		foreach ($arParams['PROPERTIES'][$CHARACTER_CODE]['VALUE'] as $IMAGES_ID) {
			$arResult['IMAGES'][$IMAGES_ID]['PREW'] = CFile::ResizeImageGet($IMAGES_ID, array(
				'width'  => $prewWidth,
				'height' => $prewHeight
			), $RESIZE_TYPE_ARR[$arParams['RESIZE_TYPE']], true);
			
			$arResult['IMAGES'][$IMAGES_ID]['BIG'] = CFile::ResizeImageGet($IMAGES_ID, array(
				'width'  => $bigWidth,
				'height' => $bigHeight
			));
		}
		
		$this->IncludeComponentTemplate();
	}
}