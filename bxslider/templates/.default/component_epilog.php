<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

CJSCore::Init(array("jquery"));

if (SM_VERSION < "15.5.1") {

	global $APPLICATION;

	if ($arParams['MODAL_WINDOW'] == 'Y') {	
		$APPLICATION->SetAdditionalCSS($templateFolder . '/js/fancybox/jquery.fancybox-1.3.4.css');
		$APPLICATION->AddHeadScript($templateFolder . '/js/fancybox/jquery.fancybox-1.3.4.pack.js');
	}

	$APPLICATION->SetAdditionalCSS($templateFolder . '/js/bxslider/jquery.bxslider.min.css');
	$APPLICATION->AddHeadScript($templateFolder . '/js/bxslider/jquery.bxslider.min.js');
} else {

    $template = & $this->GetTemplate();

    if ($arParams['MODAL_WINDOW'] == 'Y') {
        $template->addExternalCss($templateFolder . "/js/fancybox/jquery.fancybox-1.3.4.css");
        $template->addExternalJS($templateFolder . "/js/fancybox/jquery.fancybox-1.3.4.pack.js");
    }

    $template->addExternalCss($templateFolder . "/js/bxslider/jquery.bxslider.min.css");
    $template->addExternalJS($templateFolder . "/js/bxslider/jquery.bxslider.min.js");
}