<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<ul class="bxslider" id="bxslider-<? echo $arResult['ID']; ?>">
    <? foreach ($arResult['IMAGES'] as $IMAGE) { ?>
    <li>
        <? if ($arResult['MODAL_WINDOW'] == 'N') { ?>
            <img src="<? echo $IMAGE['PREW']['src']; ?>" width="<? echo $IMAGE['PREW']['width']; ?>" height="<? echo $IMAGE['PREW']['height']; ?>" alt=""/>
        <? } else { ?>
            <a href="<? echo $IMAGE['BIG']['src']; ?>" data-fancybox="gallery" title="">
                <img src="<? echo $IMAGE['PREW']['src']; ?>" width="<? echo $IMAGE['PREW']['width']; ?>" height="<? echo $IMAGE['PREW']['height']; ?>" alt=""/>
            </a>
        <? } ?>
    </li>
    <? } ?>
</ul>

<script>
$(document).ready(function(){
	$('#bxslider-<? echo $arResult['ID']; ?>').bxSlider({		
		<? if (!isset($arParams['MODE_SLIDES'])): ?>
		mode: 'horizontal',
		<? else: ?>
		mode: "<? echo $arParams['MODE_SLIDES']; ?>",
		<? endif; ?>
		minSlides: <? echo $arParams['MIN_SLIDES']; ?>,
		maxSlides: <? echo $arParams['MAX_SLIDES']; ?>,
		slideWidth: <? echo $IMAGE['PREW']['width']; ?>,
		autoHover: true,
		speed: 500,
		hideControlOnEnd: true,
		<? if ($arParams['CONTROL_SLIDES'] == 'N'): ?>
		controls: false,
		<? else: ?>
		controls: true,
		<? endif; ?>
		slideMargin: 10,
		<? if ($arParams['PAGINATION_SLIDES'] == 'Y'): ?>
		pager: true,
		<? else: ?>
		pager: false,
		<? endif; ?>
		<? if ($arParams['AUTO_SLIDES'] == 'N'): ?>
		auto: false,
		<? else: ?>
		auto: true,
		<? endif; ?>
		<? if ($arParams['INFINITE_LOOP_SLIDES'] == 'N'): ?>
		infiniteLoop: true,
		<? else: ?>
		infiniteLoop: false,
		<? endif; ?>		
	});
});	
</script>