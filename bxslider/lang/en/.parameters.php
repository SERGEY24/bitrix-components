<?
$MESS ['CHARACTER_CODE'] = "Character code images";
$MESS ['MIN_SLIDES'] = "The minimum number of slides";
$MESS ['MAX_SLIDES'] = "The maximum number of slides";
$MESS ['IMAGE_OPTION'] = "Image sizes";
$MESS ['SLIDER_OPTION'] = "Options slider";
$MESS ['RESIZE_TYPE'] = "The type of zoom";
$MESS ['MODAL_OPTION'] = "Modal window";
$MESS ['MODAL_WINDOW'] = "To enable a modal window";
$MESS ['PAGINATION_SLIDES'] = "Pagination";
$MESS ['CONTROL_SLIDES'] = "Controls «Next» / «Prev»";
$MESS ['AUTO_SLIDES'] = "The slides will automatically transition";
$MESS ['INFINITE_LOOP_SLIDES'] = "The last slide will transition to the first slide and Vice versa";
$MESS ['MODE_SLIDES'] = "The type of transition between slides";
$MESS ['MEDIALIB'] = "Media library";