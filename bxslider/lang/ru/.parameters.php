<?
$MESS ['CHARACTER_CODE'] = "Символьный код изображений";
$MESS ['MIN_SLIDES'] = "Минимальное количество слайдов";
$MESS ['MAX_SLIDES'] = "Максимальное количество слайдов";
$MESS ['IMAGE_OPTION'] = "Размеры изображений";
$MESS ['SLIDER_OPTION'] = "Опции слайдера";
$MESS ['RESIZE_TYPE'] = "Тип масштабирования";
$MESS ['MODAL_OPTION'] = "Модальное окно";
$MESS ['MODAL_WINDOW'] = "Включить модальное окно";
$MESS ['PAGINATION_SLIDES'] = "Пагинация";
$MESS ['CONTROL_SLIDES'] = "Элементы управления «Next» / «Prev»";
$MESS ['AUTO_SLIDES'] = "Слайды будут осуществлять автоматический переход";
$MESS ['INFINITE_LOOP_SLIDES'] = "На последнем слайде будет переход на первый слайд и наоборот";
$MESS ['MODE_SLIDES'] = "Тип перехода между слайдами";
$MESS ['MEDIALIB'] = "Медиабиблиотека";