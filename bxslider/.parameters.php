<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arComponentParameters = array(
	"GROUPS" => array(
		"IMAGE_OPTION" => array(
			"NAME" => GetMessage("IMAGE_OPTION"),
		),
		"SLIDER_OPTION" => array(
			"NAME" => GetMessage("SLIDER_OPTION"),
		),
		"MODAL_OPTION" => array(
			"NAME" => GetMessage("MODAL_OPTION"),
		),
	),
	"PARAMETERS" => array(
		"MIN_SLIDES" => array(
			"PARENT" => "SLIDER_OPTION",
			"NAME" => GetMessage("MIN_SLIDES"),
			"TYPE" => "STRING",
			"MULTIPLE" => "N",
			"DEFAULT" => "2",
			"REFRESH" => "N",
		),
		"MAX_SLIDES" => array(
			"PARENT" => "SLIDER_OPTION",
			"NAME" => GetMessage("MAX_SLIDES"),
			"TYPE" => "STRING",
			"MULTIPLE" => "N",
			"DEFAULT" => "3",
			"REFRESH" => "N",
		),
		"MODE_SLIDES" => array(
			"PARENT" => "SLIDER_OPTION",
			"NAME" => GetMessage("MODE_SLIDES"),
			"TYPE" => "LIST",
			"ADDITIONAL_VALUES" => "N",
			"VALUES" =>  array(
				"horizontal" => 'Горизонтальный',
				"vertical" => 'Вертикальный',
				"fade" => 'Постепенно исчезающий',
			),
			"DEFAULT" => "horizontal",
			"REFRESH" => "N"
		),
		"PAGINATION_SLIDES" => array(
			"PARENT" => "SLIDER_OPTION",
			"NAME" => GetMessage("PAGINATION_SLIDES"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "N",
		),
		"CONTROL_SLIDES" => array(
			"PARENT" => "SLIDER_OPTION",
			"NAME" => GetMessage("CONTROL_SLIDES"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "Y",
		),
		"AUTO_SLIDES" => array(
			"PARENT" => "SLIDER_OPTION",
			"NAME" => GetMessage("AUTO_SLIDES"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "Y",
		),
		"INFINITE_LOOP_SLIDES" => array(
			"PARENT" => "SLIDER_OPTION",
			"NAME" => GetMessage("INFINITE_LOOP_SLIDES"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "Y",
		),
		"BIG_SIZE" => array(
			"PARENT" => "IMAGE_OPTION",
			"NAME" => 'Большое изображение',
			"TYPE" => "STRING",
			"MULTIPLE" => "N",
			"DEFAULT" => "800x600",
			"REFRESH" => "N",
		),
		"PREW_SIZE" => array(
			"PARENT" => "IMAGE_OPTION",
			"NAME" => 'Превью изображения',
			"TYPE" => "STRING",
			"MULTIPLE" => "N",
			"DEFAULT" => "250x187",
			"REFRESH" => "N",
		),
		"RESIZE_TYPE" => array(
			"PARENT" => "IMAGE_OPTION",
			"NAME" => GetMessage("RESIZE_TYPE"),
			"TYPE" => "LIST",
			"ADDITIONAL_VALUES" => "N",
			"VALUES" =>  array(
				"BX_RESIZE_IMAGE_EXACT",
				"BX_RESIZE_IMAGE_PROPORTIONAL",
				"BX_RESIZE_IMAGE_PROPORTIONAL_ALT",
			),
			"DEFAULT" => "BX_RESIZE_IMAGE_EXACT",
			"REFRESH" => "N"
		),
		"MODAL_WINDOW" => array(
			"PARENT" => "MODAL_OPTION",
			"NAME" => GetMessage("MODAL_WINDOW"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "Y",
		),		
		'CACHE_TIME'  =>  array('DEFAULT' => 360000),
	),
);

$PARAMETERS = array();

if(\Bitrix\Main\Loader::includeModule('iblock')) {

    $properties = CIBlockProperty::GetList(
        array(
            "sort" => "asc",
            "name" => "asc"
        ),
        array(
            "ACTIVE" => "Y",
			"PROPERTY_TYPE" => "F",
			"MULTIPLE" => "Y",
        )
    );

    $PROP_DEFAULT = array('IMAGES', 'MORE_PHOTO');
    $PROPERTIES_IMAGES = array();
    
    while ($prop = $properties->GetNext()) {
        $PROPERTIES_IMAGES[$prop['CODE']] = $prop['NAME'] . ' (' . $prop['CODE'] . ')';
    }

    $PARAMETERS['CHARACTER_CODE'] = array(
        'DEFAULT' => '',
        'VALUES' => $PROPERTIES_IMAGES,
    );

    foreach ($PROPERTIES_IMAGES as $CODE) {
        if (in_array($CODE, $PROP_DEFAULT)) {
            $PARAMETERS['CHARACTER_CODE'] = array_merge($PARAMETERS['CHARACTER_CODE'], array(
                'DEFAULT' => $CODE,
            ));
            break;
        }
    }

    $PARAMETERS['CHARACTER_CODE'] = array_merge($PARAMETERS['CHARACTER_CODE'], array(
        "PARENT" => "BASE",
        "NAME" => GetMessage("CHARACTER_CODE"),
        "TYPE" => "LIST",
        "ADDITIONAL_VALUES" => "Y",
        "REFRESH" => "N",
    ));
}

if (\Bitrix\Main\Loader::includeModule('fileman')) {
	
	CMedialib::Init();
	
	$arCollections = CMedialibCollection::GetList(
		array(
			'arOrder' => array('NAME'=>'ASC'),
			'arFilter' => array('ACTIVE' => 'Y', 'TYPE' => 'image'),
		)
	); 
	
	if (!empty($arCollections)) {
		
		$MEDIALIB_LIST = array();
		
		foreach ($arCollections as $arCollection) {
			$MEDIALIB_LIST[$arCollection['ID']] = $arCollection['NAME'];
		}
		
		$PARAMETERS['MEDIALIB'] = array(
			'DEFAULT' => '',
			'VALUES' => $MEDIALIB_LIST,
		);
		
		$PARAMETERS['MEDIALIB'] = array_merge($PARAMETERS['MEDIALIB'], array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("MEDIALIB"),
			"TYPE" => "LIST",
			"ADDITIONAL_VALUES" => "Y",
			"REFRESH" => "N",
		));
	}
}

$arComponentParameters['PARAMETERS'] = array_merge($arComponentParameters['PARAMETERS'], $PARAMETERS);